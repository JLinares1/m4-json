import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class Main {
    static String jsonSource = "/home/adaw/Escritorio/m4/json/m4-json/Json/Empleats.json";

    public static void main(String[] args) throws FileNotFoundException {
        /*
         * Fes les següents activititats amb Json:
         * 1. Realitza un programa que llegeixi de teclat un departament
         * i visualitzar els seus empleats (funcio veremple). Utilitza l'entrada
         * estàndard.
         * El codi per a l'entrada estàndard és el següent
         */
        // primero un programa que lea el sistema de datos.

        // addEmpleats();

        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream(jsonSource)));
        JsonParser jsonParser = new JsonParser();
        JsonArray userarray = jsonParser.parse(reader).getAsJsonArray();

        ArrayList<Empleats> empleados = new ArrayList<>();
        for (JsonElement aUser : userarray) {

            Empleats empleaditos = gson.fromJson(aUser, Empleats.class);
            empleados.add(empleaditos);

        }
        for (Empleats tUser : empleados) {

            System.out.println(tUser);

        }

        // Empleats empleats;
        // Empleats empleat = gson.fromJson(json, Empleats.class);
        // json = gson.toJson(empleats);

        // System.out.println(empleats.toString());
    }

    public static void addEmpleats() {
        Empleats empleat = new Empleats(0, null, null, 0, null, 0, 0);

        int numeroEmpleado;
        String apellido;
        String oficio;
        int DIR;
        Date fecha;
        int SALARIO;
        int departament;
        String opcion = "";
        // numero de empleado
        try {
            do {
                System.out.print("Dime el numero de empleado:");
                opcion = darvalores();
            } while (opcion.length() <= 0 || !opcion.matches("[0-9]*"));

            numeroEmpleado = Integer.parseInt(opcion);
            // reiniciamos opcion para continuar reutilizando la variable
            opcion = "";
            System.out.print("\nDime el apellido del empleado: ");
            apellido = darvalores();

            opcion = "";
            System.out.print("\nDime el oficio del empleado: ");
            oficio = darvalores();

            opcion = "";
            do {
                System.out.print("\nDime el numero DIR del empleado: ");

                opcion = darvalores();
            } while (opcion.length() <= 0 || !opcion.matches("[0-9]*"));
            DIR = Integer.parseInt(opcion);

            opcion = "";
            // mejorar la fecha
            System.out.print("\nDime el fecha del contrato que deseas añadir: ");

        } catch (Exception e) {
            e.getMessage();
        }

    }

    public static String darvalores() throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        return input.readLine();
    }

}