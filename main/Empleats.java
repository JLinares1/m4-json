import com.google.gson.internal.bind.DefaultDateTypeAdapter.DateType;

public class Empleats {
    private int eMP_NO;
    private String aPELLIDO;
    private String oFICIO;
    private int dIR;
    private DateType fECHA;
    private int sALARIO;
    private int dEPT_NO;

    /**
     * Constructor de la clase Empleats
     * 
     */
    public Empleats() {

    }

    @Override
    public String toString() {
        return "num emp: " + this.eMP_NO + " apellido:" + this.aPELLIDO + " oficio:" + this.oFICIO + "dir:" + this.dIR
                + " fecha:" + this.fECHA + "salario" + this.sALARIO + "dept_nu:" + this.dEPT_NO;

    }

}
